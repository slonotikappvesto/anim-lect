import 'package:animationlect/screens/animation_screen.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(HomePage());
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'anim',
      home: MaterialApp(
        home: Scaffold(
            appBar: AppBar(
              title: Text('Animation'),
              centerTitle: true,
            ),
            body: AnimationScreen()),
      ),
    );
  }
}
