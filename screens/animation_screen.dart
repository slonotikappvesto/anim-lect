import 'package:flutter/material.dart';

class AnimationScreen extends StatefulWidget {
  @override
  _AnimationScreenState createState() => _AnimationScreenState();
}

class _AnimationScreenState extends State<AnimationScreen> with TickerProviderStateMixin {
  AnimationController _controller;
  AnimationController _colorAnimationController;
  AnimationController _opacityAnimationController;

  Animation<double> _size;
  Animation<double> _opacity;
  Animation<Color> _color;
  Animation<Color> _colorText;
  Animation<Color> _colorBorder;

  bool isAnim = true;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 1000),
      reverseDuration: Duration(milliseconds: 1000),
    );

    _colorAnimationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 1000),
      reverseDuration: Duration(milliseconds: 1000),
    );

    _opacityAnimationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 1500),
      reverseDuration: Duration(milliseconds: 400),
    );

    _size = Tween<double>(begin: 0, end: 100).animate(_controller);

    _color = ColorTween(begin: Colors.greenAccent, end: Colors.grey).animate(_colorAnimationController);

    _colorText = ColorTween(begin: Colors.white, end: Colors.white30).animate(_colorAnimationController);

    _colorBorder = ColorTween(begin: Colors.blue[200], end: Colors.orange[200]).animate(_colorAnimationController);

    _opacity = Tween<double>(begin: 0, end: 1).animate(_opacityAnimationController);

    _controller.addListener(() {});
    _colorAnimationController.addListener(() {});
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  void tapAnimation() {
    if (isAnim) {
      isAnim = !isAnim;
      _controller.forward();
      _colorAnimationController.forward();
      _opacityAnimationController.forward();
    } else {
      isAnim = !isAnim;
      _controller.reverse();
      _colorAnimationController.reverse();
      _opacityAnimationController.reverse();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: AnimatedBuilder(
        animation: _controller,
        builder: (BuildContext context, Widget _) {
          return Column(
            children: [
              Container(
                alignment: Alignment.center,
                width: 350,
                height: _size.value,
                color: Colors.orange,
                child: Opacity(
                  opacity: _opacity.value,
                  child: Text('AppVesto LLC', style: TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.white,
                  ),),
                ),
              ),
              Spacer(),
              InkWell(
                onTap: tapAnimation,
                child: Container(
                  alignment: Alignment.center,
                  height: 80,
                  width: 200,
                  decoration: BoxDecoration(
                    color: _color.value,
                    border: Border.all(color: _colorBorder.value, width: 3.0),
                  ),
                  child: Text(
                    'Tap me pls',
                    style: TextStyle(color: _colorText.value, fontSize: 24),
                  ),
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
